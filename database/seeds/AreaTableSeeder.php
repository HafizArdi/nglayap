<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AreaTableSeeder extends Seeder{
 
    public function run(){
        $url_province = "https://api.rajaongkir.com/starter/province?key=".env('API_KEY_PR');
        $json_str = file_get_contents($url_province);
        $json_obj = json_decode($json_str);
        $provinces = [];

        foreach($json_obj->rajaongkir->results as $province){
            $provinces[] = [
                'id' => $province->province_id,
                'name' => $province->province,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        DB::table('provinces')->insert($provinces);

        $url_city = "https://api.rajaongkir.com/starter/city?key=".env('API_KEY_PR');
        $json_str = file_get_contents($url_city);
        $json_obj = json_decode($json_str);
        $cities = [];
        foreach($json_obj->rajaongkir->results as $city){
            $cities[] = [
                'id' => $city->city_id,
                'name' => $city->type. ' ' .$city->city_name,
                'id_province' => $city->province_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        DB::table('regencies')->insert($cities);
    }
}
