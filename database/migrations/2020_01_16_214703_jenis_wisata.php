<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JenisWisata extends Migration{

    public function up(){
        Schema::create('jenis_wisatas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jenis');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('jenis_wisatas');
    }
}
