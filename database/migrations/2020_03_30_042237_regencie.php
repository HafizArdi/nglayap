<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Regencie extends Migration{
   
    public function up(){
        Schema::create('regencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('id_province');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('regencies');
    }
}
