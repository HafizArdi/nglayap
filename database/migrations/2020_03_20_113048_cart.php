<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cart extends Migration{

    public function up(){
        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idUser');
            $table->integer('idKategoriData');
            $table->integer('idjenisData');
            $table->integer('jumlah');
            $table->timestamps();
        });
    }

 
    public function down(){
        Schema::dropIfExists('cart');
    }
}
