<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Range extends Migration{

    public function up(){
        Schema::create('ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idKriteria');
            $table->string('nama');
            $table->integer('rentang');
            $table->double('bobot');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('ranges');
    }
}
