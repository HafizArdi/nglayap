<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Hotel extends Migration{

    public function up(){
        Schema::create('hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('foto');
            $table->string('nama');
            $table->integer('bintangHotel');
            $table->string('harga');
            $table->string('alamat');
            $table->integer('rating');
            $table->longText('deskripsi')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('hotels');
    }
}
