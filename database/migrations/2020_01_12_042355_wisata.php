<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Wisata extends Migration{

    public function up(){
        Schema::create('wisatas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idJenis');
            $table->string('foto');
            $table->string('nama');
            $table->string('harga');
            $table->string('alamat');
            $table->integer('rating');
            $table->longText('deskripsi')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('wisatas');
    }
}
