@extends('admin.layouts.app')
@section('content')
@section('touring', 'active')
	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Touring</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item active">Touring</li>
						</ol>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<a href="{{ route('touring.formTour') }}" type="button" class="btn btn-info float-right">Start Tour</a>
							<div id="myModal" class="modal fade" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">Start Touring</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
									</div>
								</div>
							</div>
							<div class="card-header p-2">
								<ul class="nav nav-pills">
									<li class="nav-item"><a class="nav-link active" href="#wisata" data-toggle="tab">Wisata</a></li>
									<li class="nav-item"><a class="nav-link" href="#hotel" data-toggle="tab">Hotel</a></li>
									<li class="nav-item"><a class="nav-link" href="#kuliner" data-toggle="tab">Kuliner</a></li>
								</ul>
							</div>
							<div class="card-body">
								<div class="tab-content">
									<div class="active tab-pane" id="wisata">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@foreach($wisata as $wisatas)
													<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
														<div class="card bg-light">
															<div class="card-header text-muted border-bottom-0">
																@for ($i = 1; $i <= $wisatas->rating; $i++)
																	<span class="fa fa-star checked"></span>
																@endfor
															</div>
															<div class="card-body pt-0">
																<div class="row">
																	<div class="col-7">
																		<h2 class="lead"><b>{{ $wisatas->nama }}</b></h2>
																		<p class="text-muted text-sm"><b>Deskripsi: </b> {{ $wisatas->deskripsi }} </p>
																		<ul class="ml-4 mb-0 fa-ul text-muted">
																			<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $wisatas->alamat }}</li>
																		</ul>
																	</div>
																		<div class="col-5 text-center">
																		<img src="{{asset('assets/img/wisata/'.$wisatas->foto)}}" alt="" class="img-thumbnail">
																	</div>
																</div>
															</div>
															<div class="card-footer">
																<div class="text-right">
																	<a href="{{ route('touring.detailWisata', $wisatas->id) }}" class="btn btn-sm btn-primary">
																		<i class="fas fa-eye"></i> View Detail
																	</a>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												{{ $wisata->links() }}
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="hotel">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@foreach($hotel as $hotels)
													<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
														<div class="card bg-light">
															<div class="card-header text-muted border-bottom-0">
																@for ($i = 1; $i <= $hotels->rating; $i++)
																	<span class="fa fa-star checked"></span>
																@endfor
															</div>
															<div class="card-body pt-0">
																<div class="row">
																	<div class="col-7">
																		<h2 class="lead"><b>{{ $hotels->nama }}</b></h2>
																		<p class="text-muted text-sm"><b>Deskripsi: </b> {{ $hotels->deskripsi }} </p>
																		<ul class="ml-4 mb-0 fa-ul text-muted">
																			<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $hotels->alamat }}</li>
																		</ul>
																	</div>
																		<div class="col-5 text-center">
																		<img src="{{asset('assets/img/hotel/'.$hotels->foto)}}" alt="" class="img-thumbnail">
																	</div>
																</div>
															</div>
															<div class="card-footer">
																<div class="text-right">
																	<a href="{{ route('touring.detailHotel', $hotels->id) }}" class="btn btn-sm btn-primary">
																		<i class="fas fa-eye"></i> View Detail
																	</a>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												{{ $hotel->links() }}
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="kuliner">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@foreach($kuliner as $kuliners)
													<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
														<div class="card bg-light">
															<div class="card-header text-muted border-bottom-0">
																@for ($i = 1; $i <= $kuliners->rating; $i++)
																	<span class="fa fa-star checked"></span>
																@endfor
															</div>
															<div class="card-body pt-0">
																<div class="row">
																	<div class="col-7">
																		<h2 class="lead"><b>{{ $kuliners->nama }}</b></h2>
																		<p class="text-muted text-sm"><b>Deskripsi: </b> {{ $kuliners->deskripsi }} </p>
																		<ul class="ml-4 mb-0 fa-ul text-muted">
																			<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $kuliners->alamat }}</li>
																		</ul>
																	</div>
																		<div class="col-5 text-center">
																		<img src="{{asset('assets/img/kuliner/'.$kuliners->foto)}}" alt="" class="img-thumbnail">
																	</div>
																</div>
															</div>
															<div class="card-footer">
																<div class="text-right">
																	<a href="{{ route('touring.detailKuliner', $kuliners->id) }}" class="btn btn-sm btn-primary">
																		<i class="fas fa-eye"></i> View Detail
																	</a>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												{{ $kuliner->links() }}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection