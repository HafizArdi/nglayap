@extends('admin.layouts.app')
@section('content')
@section('wisata','active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Wisata</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('wisata.index') }}">Wisata</a></li>
                            <li class="breadcrumb-item active">Add Wisata</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @if (session('status'))
                            <div class="col-md-12">
                                <div class="card bg-gradient-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Success</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        {{ session('status')}}
                                    </div>
                                </div>
                            </div>
                         @elseif(session('danger'))
                            <div class="col-md-12">
                                <div class="card bg-gradient-danger">
                                    <div class="card-header">
                                        <h3 class="card-title">Peringatan</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    {{ session('danger')}}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            <form role="form" action="{{ route('wisata.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Foto</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" name="foto" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nama Wisata</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Wisata" name="nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="harga">Harga</label>
                                        <input type="number" class="form-control" id="harga" placeholder="Masukkan Harga" name="harga">
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Wisata</label>
                                        <select class="custom-select" name="jenisWisata">
                                            <option>Pilih Jenis Wisata</option>
                                            @foreach($jenisWisata as $jenisWisatas)
                                                <option value="{{ $jenisWisatas->id }}">{{ $jenisWisatas->jenis }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Rating</label>
                                        <select class="custom-select" name="rating">
                                            <option>Pilih Rating</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" name="alamat">
                                    </div>
                                    <div class="form-group">
                                    <label for="harga">Deskripsi</label>
                                        <div class="mb-3">
                                            <textarea  name="deskripsi" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
@endsection

