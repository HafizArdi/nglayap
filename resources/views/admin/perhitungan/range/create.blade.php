@extends('admin.layouts.app')
@section('content')
@section('perhitungan','active')
@section('range', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Range</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('perhitungan.indexRange') }}">Range</a></li>
                            <li class="breadcrumb-item active">Add Range</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            <form role="form" action="{{ route('perhitungan.storeRange') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <select class="custom-select" name="idKriteria">
                                            <option>Pilih Kriteria</option>
                                            @foreach($kriteria as $kriterias)
                                                <option value="{{ $kriterias->id }}">{{ $kriterias->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nama Range</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Range" name="nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Rentang (Masukkan Rentangan Terjauh ex:20-30 maka ditulis 30)</label>
                                        <input type="number" class="form-control" id="name" placeholder="Masukkan Rentangan" name="rentang">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Bobot Rentang (Masukkan Bobot Rentang sesuai dengan tingkat kepentingan dan tidak boleh dari 1 ex: 0.2</label>
                                        <input type="number" step="0.01" min="0" max="10" class="form-control" id="name" placeholder="Masukkan Bobot" name="bobot">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
@endsection

