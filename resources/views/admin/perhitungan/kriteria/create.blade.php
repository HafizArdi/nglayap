@extends('admin.layouts.app')
@section('content')
@section('perhitungan','active')
@section('kriteria', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Kriteria</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('perhitungan.indexKriteria') }}">Kriteria</a></li>
                            <li class="breadcrumb-item active">Add Kriteria</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            <form role="form" action="{{ route('perhitungan.storeKriteria') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Kriteria" name="nama">
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori</label>
                                        <select class="custom-select" name="kategori">
                                            <option>Pilih Kategori</option>
                                            <option value="Benefit">Benefit</option>
                                            <option value="Cost">Cost</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Satuan</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Satuan" name="satuan">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
@endsection

