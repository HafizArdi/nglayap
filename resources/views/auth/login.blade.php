<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
        <title>Login &mdash; NGLAYAP</title>
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/stisla/all.css') }}" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('assets/stisla/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/stisla/css/components.css') }}">
    </head>
    <body>
        <div id="app">
            <section class="section">
                <div class="d-flex flex-wrap align-items-stretch">
                    <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
                        <div class="p-4 m-3">
                            <img src="{{ asset('assets/img/logo/logo4.png') }}" alt="logo" width="50%" class="mb-5 mt-2">
                            <h4 class="text-dark font-weight-normal">Welcome to <span class="font-weight-bold">NGLAYAP</span></h4>
                            @isset($url)
                                <form class="form-signin" method="POST" action='{{ url("login/$url") }}'>
                            @else
                            <form method="POST" action="{{ route('loginValidation') }}" class="needs-validation" novalidate="">
                            @endisset
                            {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                                    <div class="invalid-feedback">
                                        Please fill out this field
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Password</label>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                                    <div class="invalid-feedback">
                                        Please fill out this field
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                    <label class="custom-control-label" for="remember-me">Remember Me</label>
                                    </div>
                                </div>

                                <div class="form-group text-right">
                                    <a href="auth-forgot-password.html" class="float-left mt-3">
                                        Forgot Password?
                                    </a>
                                    <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
                                        Login
                                    </button>
                                </div>

                                <div class="mt-5 text-center">
                                    Don't have an account? <a href="auth-register.html">Create new one</a>
                                </div>
                            </form>
                            <div class="text-center mt-5 text-small">
                                <div class="mt-2">
                                    <a href="#">Privacy Policy</a>
                                    <div class="bullet"></div>
                                    <a href="#">Terms of Service</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100  position-relative overlay-gradient-bottom" data-background="{{ asset('assets/img/bromo.jpg') }}">
                        <div class="absolute-bottom-left index-2">
                            <div class="text-light p-5 pb-2">
                                <div class="mb-5 pb-3">
                                    <h1 class="mb-2 display-4 font-weight-bold">Start Your Amazing Trip</h1>
                                    <h5 class="font-weight-normal text-muted-transparent">Bromo, Indonesia</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script src="{{ asset('assets/stisla/jquery-3.3.1.min.js') }}" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/stisla/popper.min.js') }}" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/stisla/bootstrap.min.js') }}" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/stisla/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('assets/stisla/moment.min.js') }}"></script>
        <script src="{{ asset('assets/stisla/js/stisla.js') }}"></script>
        <script src="{{ asset('assets/stisla/js/scripts.js') }}"></script>
        <script src="{{ asset('assets/stisla/js/custom.js') }}"></script>
    </body>
</html>

