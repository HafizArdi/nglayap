@extends('customer.layouts.app')
@section('content')
@section('service', 'active')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area" id="service">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="banner_content">
                            <h1>TRAVELING</h1>
                            <p>IS EASY AND FUN</p>
                            <!-- <div class="btn">
                                <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                            </div> -->
                        </divc>
                    </div>
                </div>
            </div>
        </div>
    </section><br>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="main_title">
                        <div class="line"></div>
                        <h2>SERVICES</h2>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <img class="icon" src="{{asset('assets/img/resort.svg') }}" style="height:150px; display: block; margin: auto; margin-bottom:50px;">
                    <center><h3>Hotel Bookings</h3></center><br>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat sint exercitationem, et maxime perspiciatis libero eius error sit eaque qui, commodi tempore dolorem aperiam cupiditate asperiores repellendus in deserunt unde.</p>
                </div>
                <div class="col-6 col-md-4">
                    <img class="icon" src="{{asset('assets/img/beach.svg') }}" style="height:150px; display: block; margin: auto; margin-bottom:50px;">
                    <center><h3>Best Trip</h3></center><br>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat sint exercitationem, et maxime perspiciatis libero eius error sit eaque qui, commodi tempore dolorem aperiam cupiditate asperiores repellendus in deserunt unde.</p>
                </div>
                <div class="col-6 col-md-4">
                    <img class="icon" src="{{asset('assets/img/lifeInsurance.svg') }}" style="height:150px; display: block; margin: auto; margin-bottom:50px;">
                    <center> <h3>Insurance For Tourists</h3></center><br>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat sint exercitationem, et maxime perspiciatis libero eius error sit eaque qui, commodi tempore dolorem aperiam cupiditate asperiores repellendus in deserunt unde.</p>
                </div>  
            </div><br>
        </div>
    </section>
@endsection
