@extends('customer.layouts.app')
@section('extrahead')
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
@section('trip', 'active')
    <section class="home_banner_area" id="trip">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="banner_content">
                            <h1>TRAVELING</h1>
                            <p>IS EASY AND FUN</p>
                            <!-- <div class="btn">
                                <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                            </div> -->
                        </divc>
                    </div>
                </div>
            </div>
        </div>
    </section><br>
    <section class="content">
        <div class="card card-solid">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="col-12">
                            <img src="{{asset('assets/img/wisata/'.$detail->foto)}}" class="product-image" alt="Product Image">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        @for ($i = 1; $i <= $detail->rating; $i++)
                            <span class="fa fa-star checked"></span>
                        @endfor
                        <h3 class="my-3">{{ $detail->nama }}</h3><br>
                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                        <p>{{ $detail->alamat }}</p>
                        <strong><i class="far fa-file-alt mr-1"></i>Deskripsi</strong>
                        <p>{{ $detail->deskripsi }}</p>
                        <hr>
                        <div class="bg-gray py-2 px-3 mt-4">
                            <h2 class="mb-2">
                                Rp. {{ $detail->harga }}
                            </h2>
                        </div><br>
                        <form method="post">
                            @if(Auth::check())
                                <input type="hidden" value="{{ Auth::user()->id }}" name="idUser"> 
                                <input type="hidden" value="1" name="idKategoriData"> 
                                <input type="hidden" value="{{ $detail->id }}" name="idJenisData"> 
                                <input type="number" min="1" value="1" placeholder="Jumlah pemesanan" name="jumlah" required>
                                <button type="submit" class="btn btn-info cart">
                                    <i class="fas fa-shopping-cart"></i> Add to cart
                                </button>
                            @else
                                <button type="submit" class="btn btn-info cart">
                                    <i class="fas fa-shopping-cart"></i> Add to cart
                                </button>
                            @endif
                        </form>
                    </div>
                </div><br>
                <section class="content">
                    <div class="card card-solid">
                        <div class="container-fluid">
                            <div class="row mt-4">  
                                <div class="col-12">
                                    <h4>Recent Activity</h4><br>
                                    <div class="post">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm" src="{{ asset('assets/dist/img/user1-128x128.jpg') }}" alt="user image">
                                            <span class="username">
                                                <a href="#">Jonathan Burke Jr.</a>
                                            </span>
                                            <span class="description">Shared publicly - 7:45 PM today</span>
                                        </div>
                                        <p>
                                            Amazing...so beautiful
                                        </p>
                                    </div>
                                    <div class="post clearfix">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm" src="{{ asset('assets/dist/img/user7-128x128.jpg') }}" alt="User Image">
                                            <span class="username">
                                                <a href="#">Sarah Ross</a>
                                            </span>
                                            <span class="description">Sent you a message - 3 days ago</span>
                                        </div>
                                        <p>
                                            it's different with another beach in jember, because papuma has beautiful white sand 
                                            so the sea water looks cleaner
                                        </p>
                                    </div>
                                    <div class="post">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm" src="{{ asset('assets/dist/img/user1-128x128.jpg') }}" alt="user image">
                                            <span class="username">
                                                <a href="#">Jonathan Burke Jr.</a>
                                            </span>
                                            <span class="description">Shared publicly - 5 days ago</span>
                                        </div>
                                        <p>
                                            sooo beautiful
                                        </p>
                                    </div><br>
                                    <form class="form-horizontal">
                                        <div class="input-group input-group-sm mb-0">
                                            <textarea  name="deskripsi" placeholder="Type a comment" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #666; padding: 10px;"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary float-right">Send</button>
                                    </form>  
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection
@section('extrascript')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script>
    $('.cart').click(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        e.preventDefault();

        var idUser = $("input[name=idUser]").val();
        var idKategoriData = $("input[name=idKategoriData]").val();
        var idJenisData = $("input[name=idJenisData]").val();
        var jumlah = $("input[name=jumlah]").val();
        
        $.ajax({
            type: "POST",
            url: "{{route('cart.create')}}",
            data: {idUser:idUser, idKategoriData:idKategoriData, idJenisData:idJenisData, jumlah:jumlah},
            dataType: 'json',
            success : function (data) {
                toastr.success('Berhasil Ditambahkan!')
            },

            error : function(data){
                toastr.error('Anda Harus Login Terlebih Dahulu!')
            }
        });
    });
</script>
@endsection
