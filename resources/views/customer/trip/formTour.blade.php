@extends('layouts.app')
@section('content')
@section('trip','active')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area" id="trip">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="banner_content">
                            <h1>TRAVELING</h1>
                            <p>IS EASY AND FUN</p>
                            <!-- <div class="btn">
                                <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                            </div> -->
                        </divc>
                    </div>
                </div>
            </div>
        </div>
    </section><br>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <form role="form" action="{{ route('trip.dostartroute') }}" method="GET">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Masukkan Lokasi Anda Saat Ini</label>
                                        <select class="custom-select" name="lokasiAwal">
                                            <option>Pilih Lokasi</option>
                                            @foreach($lokasi as $lokasi)
                                                <option value="{{ $lokasi->id }}">{{ $lokasi->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Kategori Tour</label><br>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" name="kategori1">Wisata 
                                            <input type="checkbox" value="2" name="kategori2">Hotel
                                            <input type="checkbox" value="3" name="kategori3">Kuliner
                                        </label>
                                        <br>
                                    </div>
                                    <div class="form-group float-right">
                                        <button type="button"  id="add" class="btn btn-primary">Tambah Form Wisatawan</button>
                                        <button type="button"  id="remove" class="btn btn-danger">Hapus Form Wisatawan</button>
                                    </div><br>
                                    <div class="expandwist">
                                    <div id="looping1">
                                        <label>Wisatawan</label><br>
                                        <div class="form-group">
                                            <label>Pilih Rentang Jarak Tour yang Diinginkan Dari Lokasi Anda</label><br>
                                            <select class="custom-select" name="jarak[]">
                                                <option>Pilih Rentang Jarak</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 1 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} km)</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pilih Rentang Harga Tour yang Diinginkan</label><br>
                                            <select class="custom-select" name="rentangHarga[]">
                                                <option>Pilih Rentang Harga</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 2 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pilih Rentang Rating Tour yang Diinginkan</label><br>
                                            <select class="custom-select" name="rating[]">
                                                <option>Pilih Rentang Rating</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 3 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} )</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    </div>   
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Start Tour</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
        var no = 2;
        // var counter = 1;
        var jarak = ('<div class="form-group"><label>Pilih Rentang Jarak Tour yang Diinginkan Dari Lokasi Anda</label><br><select class="custom-select" name="jarak[]"><option>Pilih Rentang Jarak</option> @foreach($range as $ranges) @if($ranges->idKriteria == 1 ) <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option> @endif @endforeach</select></div>');
        var harga = ('<div class="form-group"><label>Pilih Rentang Harga Tour yang Diinginkan</label><br><select class="custom-select" name="rentangHarga[]"><option>Pilih Rentang Harga</option>@foreach($range as $ranges)@if($ranges->idKriteria == 2 )<option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option> @endif @endforeach</select></div>');
        var rating = ('<div class="form-group"><label>Pilih Rentang Rating Tour yang Diinginkan</label><br><select class="custom-select" name="rating[]"><option>Pilih Rentang Rating</option>@foreach($range as $ranges) @if($ranges->idKriteria == 3 )<option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} )</option> @endif @endforeach</select></div>');

		$("#add").click(function(){
            $(".expandwist").append('<br><br><div id="looping'+no+'"><label>Wisatawan '+no+'</label><br>'+jarak+harga+rating+'</div>');
            no++
        });

        $("#remove").click(function() {
            if(no>2){
                $('.expandwist').find('#looping'+(no-1)).remove();
                no--;
            } 
        });
	});
</script>
@endsection

