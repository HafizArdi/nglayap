@extends('customer.layouts.app')
@section('content')
	<section class="home_banner_area" id="trip">
		<div class="banner_inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-4"></div>
					<div class="col-lg-8">
						<div class="banner_content">
							<h1>TRAVELING</h1>
							<p>IS EASY AND FUN</p>
							<!-- <div class="btn">
								<div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
								<div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
							</div> -->
						</divc>
					</div>
				</div>
			</div>
		</div>
	</section><br>
	<div class="content">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Profile</h1>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="card card-primary card-outline">
							<div class="card-body box-profile">
								<div class="text-center">
									<img id="gambarpreview" class="profile-user-img img-fluid img-circle"
										src="@if($user->foto != null) {{ asset('assets/img/profile/'.$user->foto) }} @else {{ asset('assets/dist/img/user4-128x128.jpg') }} @endif"
										alt="User profile picture">
								</div>
								<h3 class="profile-username text-center">{{ $user->name }}</h3>
								<ul class="list-group list-group-unbordered mb-3">
									<li class="list-group-item">
										<b>Email</b> <a class="float-right">{{ $user->email }}</a>
									</li>
									<li class="list-group-item">
										@foreach($user_profile as $profile)
											<b>No Telepon</b> <a class="float-right">{{ $profile->phone }}</a>
										@endforeach
									</li>
									<li class="list-group-item">
										@foreach($user_profile as $profile)
											<b>Alamat</b><a class="float-right">{{ $profile->alamat }}</a>
										@endforeach
									</li>
								</ul>
							</div>
						</div>
		  			</div>
					<div class="col-md-9">
						<div class="card">
							<div class="card-body">
								<div class="tab-content"></div>
								<div class="tab-pane" id="settings">
									<form class="form-horizontal" action="{{ route('profile.update', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
										@csrf
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Foto</label>
											<div class="input-group col-sm-10">
												<div class="custom-file">
													<input type="file" name="foto" class="custom-file-input inputgambar" id="ingambar">
													<label class="custom-file-label ingambarlabel" for="ingambar">Choose file</label>
												</div>
												<div class="input-group-append">
													<span class="input-group-text" id="">Upload</span>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label for="inputName" class="col-sm-2 col-form-label">Nama</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputName" name="nama" placeholder="Name" value="{{ $user->name }}">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
											<div class="col-sm-10">
												<input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" value="{{ $user->email }}">
											</div>
										</div>
										<div class="form-group row">
											<label for="inputName2" class="col-sm-2 col-form-label">Kode Post</label>
											<div class="col-sm-10">
												@if(count($user_profile) != 0)
													@foreach($user_profile as $kodepost)
														<input type="number" class="form-control" id="inputName2" name="postcode" placeholder="Kode Post" value="{{ $kodepost->postcode }}">
													@endforeach
												@else
													<input type="number" class="form-control" id="inputName2" name="postcode" placeholder="Kode Post">	
												@endif
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Provinsi</label>
											<div class="col-sm-10">
												<select class="custom-select" name="provinsi" id="provinsi">
													<option disabled>Pilih Provinsi</option>
													@foreach($provinceAll as $provincesAll)
														@if(count($user_profile) != 0)
															<option {{ $user_profile[0]->userRegencies()->first()->id_province == $provincesAll->id ? 'selected' : '' }} value="{{ $provincesAll->id }}" >{{ $provincesAll->name }}</option>
														@else
															<option value="{{ $provincesAll->id }}">{{ $provincesAll->name }}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Pilih kabupaten</label>
											<div class="col-sm-10">
												<select class="custom-select" name="kabupaten" id="kabupaten">
													@foreach($regencyAll as $regenciesAll )
														@if(count($user_profile) != 0)
															<option {{ $user->id_regency == $regenciesAll->id ? 'selected' : '' }} value="{{ $regenciesAll->id }}"> {{ $regenciesAll->name }} </option>
														@endif
													@endforeach
													<option disabled value="0">- Select -</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label for="phone" class="col-sm-2 col-form-label">No. Telepon</label>
											<div class="col-sm-10">
												@if(count($user_profile) != 0)
													@foreach($user_profile as $userProfile)
														<input type="number" class="form-control" id="phone" name="phone" placeholder="No. Telepon" value="{{ $userProfile->phone }}">
													@endforeach
												@else
													<input type="number" class="form-control" id="phone" name="phone" placeholder="No. Telepon">
												@endif
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Alamat</label>
											<div class="col-sm-10">
												@if(count($user_profile) != 0)
													@foreach($user_profile as $userProfile)
														<textarea  name="alamat" placeholder="Alamat" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $userProfile->alamat }}</textarea>
													@endforeach
												@else
													<textarea name="alamat" placeholder="Alamat" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
												@endif
											</div>
										</div>
										<div class="form-group row">
											<label for="Password" class="col-sm-2 col-form-label">Password</label>
											<div class="col-sm-10">
												<input type="password" class="form-control" id="Password" name="password1" placeholder="Password">
											</div>
										</div>
										<div class="form-group row">
											<div class="offset-sm-2 col-sm-10">
												<button type="submit" class="btn btn-primary float-right">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
  	</div>
@endsection
@section('extrascript')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
	$("#provinsi").focusout(function(e){
		var idProvinsi = $(this).val();
		$.ajax({
			type: "GET",
			url: "{{ route('profile.getRegency') }}",
			data: {'idProvinsi' : idProvinsi},
			dataType: 'json',
			success: function(response){
				var length = response.length;
				$("#kabupaten").empty();

				for(var i=0; i<length; i++){
					var id = response[i]['id'];
					var name = response[i]['name'];
					$("#kabupaten").append("<option value='"+id+"'>"+name+"</option>");
				}
			},

			error: function(response){
				alert(response.responseJSON.message);
			}
		});
	});
</script>
<script type="text/javascript">
    @if($user->foto!=null)
        document.getElementsByClassName('ingambarlabel')[0].innerHTML = '{{$user->foto}}';
    @endif
    function namedefine(){
    event.preventDefault();
    var ingambar = document.getElementsByClassName('inputgambar');
    var inlabel = document.getElementsByClassName('ingambarlabel');
    if(ingambar[0].value.length){
        inlabel[0].innerHTML =  ingambar[0].files[0].name;
        var oFReader = new FileReader();
        oFReader.onload = function(oFREvent) {
            document.getElementById("gambarpreview").src = oFREvent.target.result;
        };
        oFReader.readAsDataURL(ingambar[0].files[0]);
        }
        else{
        console.log('cancelled');
        }
    }
    $("#ingambar").change(function() {
    namedefine();
    });
</script>
@endsection

