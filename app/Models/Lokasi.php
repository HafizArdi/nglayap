<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model{
    protected $fillable = [
        'nama', 'alamat',
    ];

    protected $hidden = [
        'created_at', 'update_at',
    ];

    public function dataPrediksi(){
        return $this->hasMany('App\Models\DataPrediksi', 'id', 'idLokasi');
    }

    
}
