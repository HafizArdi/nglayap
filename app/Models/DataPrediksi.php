<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataPrediksi extends Model{
    protected $fillable = [
        'idLokasi', 'idKategoriData', 'idJenisData' , 'jarak', 
    ];

    protected $hidden = [
        'created_at', 'update_at',
    ];

    public function lokasi(){
        return $this->belongsTo('App\Models\Lokasi', 'idLokasi', 'id');
    }

    public function kategoriData(){
        return $this->belongsTo('App\Models\KategoriData', 'idKategoriData', 'id');
    }

    public function wisata(){
        return $this->belongsTo('App\Models\Wisata', 'idJenisData', 'id');
    }

    public function hotel(){
        return $this->belongsTo('App\Models\Hotel', 'idJenisData', 'id');
    }

    public function kuliner(){
        return $this->belongsTo('App\Models\Kuliner', 'idJenisData', 'id');
    }
}
