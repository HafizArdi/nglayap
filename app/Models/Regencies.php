<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regencies extends Model{
    protected $fillable = [
        'name', 'id_province',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function provinces(){
        return $this->belongsTo('App\Models\Provinces', 'id_province', 'id');
    }

    public function userRegencies(){
        return $this->hasMany('App\Models\UserProfile', 'id', 'id_regency');
    }
}
