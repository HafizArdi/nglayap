<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriData extends Model{
    protected $fillable = [
        'nama',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function dataPrediksi(){
        return $this->hasMany('App\Models\DataPrediksi', 'id', 'idKategoriData');
    }

    public function cart(){
        return $this->hasMany('App\Models\Cart', 'id', 'idKategoriData');
    }
}

