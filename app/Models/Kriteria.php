<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model{
    protected $fillable = [
        'nama', 'kategori',
    ];

    public $timestamps = false;

    public function range(){
        return $this->hasMany('App\Models\Range', 'id', 'idKriteria');
    }
}
