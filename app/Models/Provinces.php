<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model{
    protected $fillable = [
        'name', 
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function regencies(){
        return $this->hasMany('App\Models\Regencies', 'id', 'id_province');
    }
}
