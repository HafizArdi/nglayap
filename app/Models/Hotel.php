<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model{
    protected $fillable = [
        'foto', 'nama', 'harga', 'bintangHotel', 'alamat', 'rating', 'deskripsi',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function dataPrediksi(){
        return $this->hasMany('App\Models\DataPrediksi', 'id', 'idJenisData');
    }

    public function cart(){
        return $this->hasMany('App\Models\Cart', 'id', 'idJenisData');
    }
}

