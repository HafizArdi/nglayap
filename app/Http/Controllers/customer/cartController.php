<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use Auth;

class cartController extends Controller{

    public function index(){
        $myCart = Cart::where('idUser', '=', Auth::user()->id)->get();
        return view('customer.cart.index', compact('myCart'));
    }

    public function create(Request $request){
        $cart = new Cart();
        if(Auth::check() && Auth::user()->hasRole('customer')){
            $cart->idUser = $request->get('idUser');
            $cart->idKategoriData = $request->get('idKategoriData');
            $cart->idJenisData = $request->get('idJenisData');
            $cart->jumlah = $request->get('jumlah');
            $cart->save();
            
            return json_encode($cart);
        } 
    }


    public function update(Request $request, $id){
        $update = Cart::find($id);
        $update->jumlah = $request->get('jumlah');
        $update->save();

        return redirect (route('cart.index'));
    }

    public function destroy($id){
        $cart = Cart::where('id',$id)->first();
        $cart->forceDelete();
        return redirect (route('cart.index'));
    }
}
