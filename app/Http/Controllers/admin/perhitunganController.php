<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\perhitunganRepository;
use App\Http\Requests\KriteriaStoreRequest;
use App\Http\Requests\KriteriaUpdateRequest;
use App\Http\Requests\RangeStoreRequest;
use App\Http\Requests\RangeUpdateRequest;
use App\Http\Requests\LokasiStoreRequest;
use App\Http\Requests\LokasiUpdateRequest;
use App\Http\Requests\DataPrediksiStoreRequest;
use App\Http\Requests\DataPrediksiUpdateRequest;
use App\Models\Kriteria;
use App\Models\Range;
use App\Models\Lokasi;
use App\Models\Wisata;
use App\Models\Hotel;
use App\Models\Kuliner;
use App\Models\KategoriData;
use App\Models\DataPrediksi;
use Illuminate\Support\Facades\DB;
use Redirect;

class perhitunganController extends Controller{
    
    private $perhitunganRepository;

    public function __construct(perhitunganRepository $perhitunganRepository){
        $this->perhitunganRepository = $perhitunganRepository;
    }
    
    // kriteria
    public function indexKriteria(){
        $kriteria = $this->perhitunganRepository->getKriteria();
        return view('admin.perhitungan.kriteria.index', compact('kriteria'));
    }

    public function createKriteria(){
        return view('admin.perhitungan.kriteria.create');
    }

    public function storeKriteria(KriteriaStoreRequest $request){
        $this->perhitunganRepository->storeKriteria($request);
        return redirect(route('perhitungan.indexKriteria'))->with('status', 'Data Kriteria Berhasil Ditambahkan');
    }

    public function editKriteria($id){
        $edit = $this->perhitunganRepository->getKriteria(null, $id);
        return view('admin.perhitungan.kriteria.edit', compact('edit'));
    }

    public function updateKriteria(KriteriaUpdateRequest $request, Kriteria $kriteria, $id){
        $this->perhitunganRepository->updateKriteria($request, $kriteria, $id);
        return redirect(route('perhitungan.indexKriteria'))->with('status', 'Data Kriteria Berhasil Dirubah');
    }

    public function destroyKriteria($id){
        $kriteria = Kriteria::where('id', $id)->first();
        $range = Range::where('idKriteria', '=', $id)->get();
        if($range->count() > 0){
            return Redirect::back()->with('danger', 'Data Range Tidak Kosong, Harap Hapus Terlebih Dahulu');
        }elseif($range->count() == 0){
            $kriteria->forceDelete();
            return redirect(route('perhitungan.indexKriteria'))->with('status', 'Data Kriteria Berhasil Dihapus');
        }
    }

    // range
    public function indexRange(){
        $range = $this->perhitunganRepository->getRange();
        return view('admin.perhitungan.range.index', compact('range'));
    }

    public function createRange(){
        $kriteria = $this->perhitunganRepository->createRange();
        return view('admin.perhitungan.range.create', compact('kriteria'));
    }

    public function storeRange(RangeStoreRequest $request){
        $this->perhitunganRepository->storeRange($request);
        return redirect(route('perhitungan.indexRange'))->with('status', 'Data Range Berhasil Ditambahkan');
    }

    public function editRange($id){
        $edit = Range::find($id);
        $kriteria = Kriteria::all();
        return view('admin.perhitungan.range.edit', compact('edit', 'kriteria'));
    }

    public function updateRange(RangeUpdateRequest $request, Range $range, $id){
        $this->perhitunganRepository->updateRange($request, $range, $id);
        return redirect(route('perhitungan.indexRange'))->with('status', 'Data Range Berhasil Dirubah');
    }

    public function destroyRange($id){
        $this->perhitunganRepository->destroyRange($id);
        return redirect(route('perhitungan.indexRange'))->with('status', 'Data Range Berhasil Di Hapus');
    }

    // lokasi awal
    public function indexLokasi(){
        $lokasi = $this->perhitunganRepository->getLokasi();
        return view('admin.perhitungan.lokasiAwal.index', compact('lokasi'));
    }

    public function createLokasi(){
        return view('admin.perhitungan.lokasiAwal.create');
    }

    public function storeLokasi(LokasiStoreRequest $request){
        $this->perhitunganRepository->storeLokasi($request);
        return redirect(route('perhitungan.indexLokasi'))->with('status', 'Data Lokasi Berhasil Ditambahkan');
    }

    public function editLokasi($id){
        $edit = $this->perhitunganRepository->getLokasi(null, $id);
        return view('admin.perhitungan.lokasiAwal.edit', compact('edit'));
    }

    public function updateLokasi(LokasiUpdateRequest $request, Lokasi $lokasi, $id){
        $this->perhitunganRepository->updateLokasi($request, $lokasi, $id);
        return redirect(route('perhitungan.indexLokasi'))->with('status', 'Data Lokasi Berhasil Dirubah');
    }

    public function destroyLokasi($id){
        $lokasi = Lokasi::where('id', $id)->first();
        $prediksi = DataPrediksi::where('idLokasi', '=', $id)->get();
        if($prediksi->count() > 0){
            return Redirect::back()->with('danger', 'Data Prediksi Tidak Kosong, Harap Hapus Terlebih Dahulu');
        }elseif($prediksi->count() == 0){
            $lokasi->forceDelete();
            return redirect(route('perhitungan.indexLokasi'))->with('status', 'Data Lokasi Berhasil Dihapus');
        }
    }

    // data prediksi
    public function indexDataPrediksi(){
        $dataPrediksi = $this->perhitunganRepository->getDataPrediksi();
        return view('admin.perhitungan.dataPrediksi.index', compact('dataPrediksi'));
    }

    public function createDataPrediksi(){
        $lokasi = Lokasi::all();
        $kategoriData = KategoriData::all();

        return view('admin.perhitungan.dataPrediksi.create', compact('lokasi', 'kategoriData'));
    }

    public function getDataPrediksi(Request $request){
        $rangeJarak = Range::where('idKriteria', 1)->get();
        $jarak = $request->get('jarak');
        $idKategoriData = $request->get('idKategoriData');

        if($idKategoriData == 1){
            $wisata = Wisata::all();
            return json_encode($wisata);
        }elseif($idKategoriData == 2){
            $hotel = Hotel::all();
            return json_encode($hotel);
        }elseif($idKategoriData==3){
            $kuliner = Kuliner::all();
            return json_encode($kuliner);
        }


        // for($i=0;$i<count($rangeJarak);$i++){
        //     if($jarak <= $rangeJarak[$i]->rentang){
        //         $idRangeJarak = $rangeJarak[$i];
            
        //         $variabel += $idRangeJarak;
        //         return json_encode($idRangeJarak);
        //     }
        // }
    }

    public function storeDataPrediksi(DataPrediksiStoreRequest $request){
        $this->perhitunganRepository->storeDataPrediksi($request);
        return redirect(route('perhitungan.indexDataPrediksi'))->with('status', 'Data Prediksi Berhasil Ditambahkan');
    }

    public function editDataPrediksi($id){
        $edit = DataPrediksi::find($id);
        $lokasi = Lokasi::all();
        $kategoriData = KategoriData::all();
        return view('admin.perhitungan.dataPrediksi.edit', compact('edit', 'lokasi', 'kategoriData'));
    }

    public function updateDataPrediksi(DataPrediksiUpdateRequest $request, DataPrediksi $dataPrediksi, $id){
        $this->perhitunganRepository->updateDataPrediksi($request, $dataPrediksi, $id);
        return redirect(route('perhitungan.indexDataPrediksi'))->with('status', 'Data Prediksi Berhasil Dirubah');
    }

    public function destroyDataPrediksi($id){
        $this->perhitunganRepository->destroyDataPrediksi($id);
        return redirect(route('perhitungan.indexDataPrediksi'))->with('status', 'Data Prediksi Berhasil Dihapus');
    }
}

