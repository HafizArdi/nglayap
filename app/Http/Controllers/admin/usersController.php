<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Models\Role;
use App\Models\UserRole;
use Auth;
use Carbon\Carbon;
use Hash;
use Redirect;

class usersController extends Controller{

    public function index(){
        $profile = User::find(Auth::user()->id);
        $roles = Role::all();
        return view('admin.users.userProfile.index', compact('profile','roles'));
    }

    public function validator($request){
        $validator = [
            'foto' => 'image|mimes:jpeg,jpg,png',
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'password1' => 'required|max:191',
        ];

        return Validator::make($request->all(), $validator);
    }

    public function update(Request $request, $id){
        $validator = $this->validator($request);
        $detail = User::find($id);
        if($validator->passes()){
            if($request->file('foto')!=null){
                if($detail->foto!=null){
                    $imagepath=public_path().'/assets/img/profile/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/profile/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }
                $detail->name = $request->get('name');
                $detail->email = $request->get('email');
                $detail->password = Hash::make($request->password1);
                $detail->foto = $newname;

                if($detail->save()){
                    $roleedit = $detail->userrole()->first();
                    if($roleedit->role_id==1){
                        $roleedit->role_id = $roleedit->role_id;
                    }
                    else{
                        $roleedit->role_id = $request->roles;
                    }
                    $roleedit->save();
                }
                    return redirect(route('users.userProfile'))->with('status', 'Data Profile Berhasil Dirubah');
        }else{
            return Redirect::back()->with('danger', 'Harap Lengkapi Data!');
        }
    }

    public function indexUserAccount(){
        $userAccount = User::with('userrole')->get();
        return view('admin.users.userAccount.index', compact('userAccount'));
    }

    public function addUserAccount(){
        $roles = Role::all();
        return view('admin.users.userAccount.create', compact('roles'));
    }

    public function storeUserAccount(Request $request){
        $validator = $this->validator($request);
        $detail = new User();

        if($validator->passes()){
            if($request->file('foto')!=null){
                if($detail->foto!=null){
                    $imagepath=public_path().'/assets/img/profile/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/profile/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }
                $detail->name = $request->get('name');
                $detail->email = $request->get('email');
                $detail->password = Hash::make($request->password1);
                $detail->foto = $newname;

                if($detail->save()){
                    $getid = $detail->id;
                    $insertrole = ([
                        'user_id' => $getid,
                        'role_id' => $request->roles,
                    ]);
                    $roles = new UserRole($insertrole);
                    $roles->save();
                }
                    return redirect(route('users.account'))->with('status', 'Data Account Berhasil Ditambahkan');
        }else{
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

    public function editUserAccount($id){
        $userAccount = User::find($id);
        $roles = Role::all();
        return view('admin.users.userAccount.edit', compact('userAccount', 'roles'));
    }

    public function updateUserAccount(Request $request, $id){
        $validator = $this->validator($request);
        $detail = User::find($id);
        if($validator->passes()){
            if($request->file('foto')!=null){
                if($detail->foto!=null){
                    $imagepath=public_path().'/assets/img/profile/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/profile/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }
                $detail->name = $request->get('name');
                $detail->email = $request->get('email');
                $detail->password = Hash::make($request->password1);
                $detail->foto = $newname;

                if($detail->save()){
                    $roleedit = $detail->userrole()->first();
                    if($roleedit->role_id==1){
                        $roleedit->role_id = $roleedit->role_id;
                    }
                    else{
                        $roleedit->role_id = $request->roles;
                    }
                    $roleedit->save();
                }
                    return redirect(route('users.account'))->with('status', 'Data Account Berhasil Dirubah');
        }else{
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

    public function destroyUserAccount($id){
		$user = User::where('id','=', $id)->first();
		$user->forceDelete();
		return Redirect::back();
	}
    
}
