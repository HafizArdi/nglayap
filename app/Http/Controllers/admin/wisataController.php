<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Wisata;
use Carbon\Carbon;
use App\Repositories\wisataRepository;
use App\Http\Requests\WisataStoreRequest;
use App\Http\Requests\WisataUpdateRequest;
use App\Models\JenisWisata;
use App\Models\DataPrediksi;
use Redirect;

class wisataController extends Controller{

    private $wisataRepository;

    public function __construct(wisataRepository $wisataRepository){
        $this->wisataRepository = $wisataRepository;
    }

    public function index(){
        $wisata = $this->wisataRepository->get();
        return view('admin.wisata.index',compact('wisata'));
    }

    public function create(){
        $jenisWisata = JenisWisata::all();
        return view('admin.wisata.create', compact('jenisWisata'));
    }

    public function store(WisataStoreRequest $request){
        $this->wisataRepository->store($request);
        return redirect(route('wisata.index'))->with('status', 'Data Wisata Berhasil Ditambahkan'); 
    }

    public function edit($id){
        $edit = Wisata::find($id);
        $jenisWisata = JenisWisata::all();
        return view('admin.wisata.edit', compact('edit', 'jenisWisata'));
    }

    public function update(WisataUpdateRequest $request, Wisata $wisata, $id){
        $this->wisataRepository->update($request, $wisata, $id);
        return redirect(route('wisata.index'))->with('status', 'Data Wisata Berhasil Dirubah');
    }

    public function destroy($id){
        $wisata = Wisata::where('id','=', $id)->first();
        $prediksi = DataPrediksi::where('idLokasi', '=', $id)->get();
        if($prediksi->count() > 0){
            return Redirect::back()->with('danger', 'Data Prediksi Tidak Kosong, Harap Hapus Terlebih Dahulu');
        }elseif($prediksi->count() == 0){
            $wisata->forceDelete();
            return redirect(route('wisata.index'))->with('status', 'Data Wisata Berhasil Dihapus');
        }
    }

}
