<?php
namespace App\Repositories;

use App\Models\Wisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class wisataRepository{
    private $model;

    public function __construct(Wisata $model){
        $this->model = $model;
    }

    public function get($pagination = null, $with = null){
        $wisata = $this->model
            ->when($with, function ($query) use ($with) {
                return $query->with($with);
            });
        
        if($pagination){
            return $wisata->paginate($pagination);
        }

        return $wisata->get();
    }

    public function store(Request $request){
        DB::beginTransaction();
        $detail = new Wisata();

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/wisata/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/wisata/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $wisata = Wisata::create([
                'idJenis' => $request->get('jenisWisata'),
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'rating' => $request->get('rating'),
                'alamat' => $request->get('alamat'),
                'deskripsi' => $request->get('deskripsi'),
            ]);
            DB::commit();
            return $wisata;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function update(Request $request, Wisata $wisata, $id){
        
        DB::beginTransaction();
        $detail = Wisata::find($id);

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/wisata/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/wisata/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $wisata->where('id',$id)->update([
                'idJenis' => $request->get('jenisWisata'),
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'alamat' => $request->get('alamat'),
                'rating' => $request->get('rating'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit(); 
            return $wisata;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }
}